document.oncontextmenu=RightMouseDown;
document.onmousedown = mouseDown;

  function mouseDown(e) {
      if (e.which==3) {//righClick
      alert("Cuidado com a desclassificação!");
   }
}

function RightMouseDown() { return false;}

$(function () {
  "use strict";

var pontuacao = 0;
var nome = "";
var tempo = 5;
var relogio = 60;

var operador = null;
var operando = null;
var resultado = null;

$("#iniciar").click(function(){
  $(".inicial").slideUp('slow');
  $(".start").slideDown('slow');
});

$("#jogar").click(function(){
  nome = $("#nome").val();
  if(nome == ""){
    alert("Esqueceu seu nome?");
  }else{
    iniciaPartida(nome);
  }
});

$("#form-resposta").on("submit",function(e){
  e.preventDefault();

  var resposta = $("#resposta").val();
  if(resposta == resultado && relogio > 0){
    escolheNumero();
    pontuacao++;
    $("#acertos").val(pontuacao);
    console.log("Pontuacao: "+pontuacao);
    $("#resposta").val("");
    $("#resposta").focus();
  }else{
    $("#resposta").effect("shake");
    $("#resposta").val("");
    $("#resposta").focus();
  }

});

$(".responder").on("click",function(e){
  e.preventDefault();

  var resposta = $("#resposta").val();
  if(resposta == resultado  && relogio > 0){
    escolheNumero();
    pontuacao++;
    $("#acertos").val(pontuacao);
    $("#resposta").val("");
    $("#resposta").focus();
  }else{
    $("#resposta").effect("shake");
    $("#resposta").val("");
    $("#resposta").focus();
  }

});


function iniciaPartida(nome){
  $(".main").hide('slow');
  $(".jogo").show('slow');

  // Chama a função ao carregar a tela
  startCountdown();
}


function startCountdown(){
	// Se o tempo não for zerado
	if((tempo - 1) >= 0){
		// Calcula os segundos restantes
		var seg = tempo%60;
		if(seg <=9){
			seg = "0"+seg;
		}

		// Cria a variável para formatar no estilo hora/cronômetro
		var horaImprimivel = seg;
		//JQuery pra setar o valor
		$(".operacao span").html(horaImprimivel);

		// Define que a função será executada novamente em 1000ms = 1 segundo
		window.setTimeout(startCountdown,1000);

		// diminui o tempo
		tempo--;

	// Quando o contador chegar a zero faz esta ação
	} else {
    $(".operacao").html('');
    $("#form-resposta").show();
    jogar();
	}

}

function contaRelogio(){

	if((relogio - 1) >= 0){
		var seg = relogio%61;
		if(seg <=9){
			seg = "0"+seg;
		}

		var horaImprimivel = seg;

		$(".progress-bar").html(horaImprimivel);
		$(".progress-bar").css({'width':((seg*10)/6)+'%'});

		window.setTimeout(contaRelogio,1000);

		relogio--;

}else{
  mostraResultado();
}

}


function jogar(){
  // Mostra os paineis
  $('.pontuacao,.tempo').show('slow');
  $('.jogo .titulo').html("Valendo!");
  $( "#resposta" ).focus();
  escolheNumero();
  contaRelogio();
}

function escolheNumero(){
  operador = Math.floor((Math.random() * 5) + 5);
  operando = Math.floor((Math.random() * 9) + 1);
  resultado = operador*operando;
  $(".operacao").html(operador+"x"+operando);
}

function mostraResultado(){
  $('.jogo,.pontuacao,.tempo').hide('slow');
  $('.resultado').show('slow');
  $(".resultado .text-info span").html(nome);
  $(".resultado .text-danger span").html(pontuacao);
}

});
